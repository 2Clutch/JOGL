## 🔆 Thank you for your ideas.


### Please give your feature a name
name: 

### Which site is it linked to?
project? community? Paste the URL here where you had the idea for this feature.

### Summarise the feature
in very short: 

* What's already there?
* What would you love to see? 
* & Why?


### Label your feature request
Please add the label 'feature request' from the drop-down list below this text field and click 'submit'.


## ✨ Thank you! Your feature has been submitted and we will look into how we could implement it!  ✨ 

