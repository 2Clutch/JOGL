## 🐞 Thank you for your feedback.

Please have a quick check through our [open bug reports](https://gitlab.com/JOGL/JOGL/issues?label_name%5B%5D=Bug&scope=all&sort=weight&state=opened&utf8=%E2%9C%93) if your problem has already been reported.  
If so, you can click :thumbsup: to indicate that you have the same issue; and you can add a comment to the issue give more detail.  
If not, go ahead and create this new issue.

### Please name your issue
starting with [BETA TESTING] and then the bug title

### Summarise the bug
in very short: 

### Steps to reproduce the issue
* What did you do in which sequence?
* Do not include sensitive information.
    
### Current behaviour
* Upload the browser error message if you are familiar with web developer tools.
* You can also add a screenshot capturing the bug and attach it here (drag&drop).

### Expected behaviour
* What should ideally happen? 

### Version information
* **Browser + version**:
* **Your OS + version**:
* **Device used**: (phone/tablet/desktop)


### Label your issue
Please add the label 'bug' from the drop-down list below this text field and click 'submit'.


## ✨ Thank you! Your bug has been reported and we will work hard to fix it asap!  ✨

